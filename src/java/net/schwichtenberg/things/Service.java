/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.schwichtenberg.things;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 *
 * @author fschwic
 */
@Path("/things")
@Stateless
public class Service {

    @PersistenceContext
    private EntityManager entityManager;

    @GET
    @Path("{id}")
    public SomeThing get(@PathParam("id") Long id) {
        SomeThing result = entityManager.find(SomeThing.class, id);
        if(result == null){
            throw new NotFoundException("SomeThing with id " + id + " not found.");
        }
        // System.out.println(result.getId() + " " + result.getName() + " " + result.getNote());        
        return result;
    }
    
    @GET
    public List<SomeThing> getAll(){
        TypedQuery<SomeThing> q = entityManager.createQuery("SELECT t FROM SomeThing t", SomeThing.class);
        return q.getResultList();
    }

    @POST
    public SomeThing create(SomeThing st) {
        System.out.println(st.getId() + " " + st.getName() + " " + st.getNote());
        entityManager.persist(st);
        return st;
    }

    @PUT
    @Path("{id}")
    public SomeThing update(@PathParam("id") Long id, SomeThing st){
        if(!id.equals(st.getId())){
            throw new BadRequestException("Primary key (id) can not be updated.");
        }
        entityManager.merge(st);
        return st;
    }
    
    @DELETE
    @Path("{id}")
    public void delete(@PathParam("id") Long id){
        SomeThing st = this.get(id);
        System.out.println(st);
        entityManager.remove(st);
    }
    
}
